package br.com.ramboindustries.curso.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.ramboindustries.curso.domain.enums.EstadoPagamento;

@Entity
@Table(name = "PagamentosComCartao")
public class PagamentoComCartao extends Pagamento  {
	
	private static final long serialVersionUID = 3373226800245244855L;

	@Column(name = "NumeroDeParcelas")
	private Integer numeroDeParcelas;
	
	public PagamentoComCartao() {
		
	}

	public PagamentoComCartao(Integer pagamentoId, EstadoPagamento estadoPagamento, Pedido pedido, Integer numeroDeParcelas) {
		super(pagamentoId, estadoPagamento, pedido);
		this.numeroDeParcelas = numeroDeParcelas;
	}

	public Integer getNumeroDeParcelas() {
		return numeroDeParcelas;
	}

	public void setNumeroDeParcelas(Integer numeroDeParcelas) {
		this.numeroDeParcelas = numeroDeParcelas;
	}
	
	
	
	

}
