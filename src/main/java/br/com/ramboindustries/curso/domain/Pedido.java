package br.com.ramboindustries.curso.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Pedidos")
public class Pedido implements Serializable {

	private static final long serialVersionUID = 2369129919585202967L;

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "PedidoId")
	private Integer pedidoId;

	@Column(name = "Instante")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date instante;

	@ManyToOne
	@JoinColumn(name = "ClienteId")
	private Cliente cliente;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "pedido")
	private Pagamento pagamento;
	/**
	 * Na classe Pagamento temos um atributo chamado pedido, que é dona do
	 * relacionamento OneToOne entre Pagamento & Pedido
	 */

	@ManyToOne
	@JoinColumn(name = "EnderecoId")
	private Endereco enderecoEntrega;

	/**
	 * Como estamos embutindo o id chamamos a o atributo do ID, que na classe
	 * ItemPedidoPk temos um atributo chamado pedido
	 */
	@OneToMany(mappedBy = "itemPedidoPk.pedido")
	private Set<ItemPedido> itens = new HashSet<>();

	public Pedido() {

	}

	public Pedido(Integer pedidoId, Date instante, Cliente cliente, Endereco enderoEntrega) {
		super();
		this.pedidoId = pedidoId;
		this.instante = instante;
		this.cliente = cliente;
		this.enderecoEntrega = enderoEntrega;
	}

	/**
	 * Lembrando que será deserializado todos os métodos que tenham o get na frente!
	 */
	public double getValorTotal() {
		double soma = 0.0;
		for (ItemPedido item : itens) {
			soma += item.getSubTotal();
		}
		return soma;
	}

	public Integer getPedidoId() {
		return pedidoId;
	}

	public void setPedidoId(Integer pedidoId) {
		this.pedidoId = pedidoId;
	}

	public Date getInstante() {
		return instante;
	}

	public void setInstante(Date instante) {
		this.instante = instante;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Pagamento getPagamento() {
		return pagamento;
	}

	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}

	public Endereco getEnderecoEntrega() {
		return enderecoEntrega;
	}

	public void setEnderecoEntrega(Endereco enderecoEntrega) {
		this.enderecoEntrega = enderecoEntrega;
	}

	public Set<ItemPedido> getItens() {
		return itens;
	}

	public void setItens(Set<ItemPedido> itens) {
		this.itens = itens;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pedidoId == null) ? 0 : pedidoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		if (pedidoId == null) {
			if (other.pedidoId != null)
				return false;
		} else if (!pedidoId.equals(other.pedidoId))
			return false;
		return true;
	}

}
