package br.com.ramboindustries.curso.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.ramboindustries.curso.domain.enums.EstadoPagamento;

@Entity
@Table(name = "Pagamentos")
@Inheritance(strategy = InheritanceType.JOINED) // ira criar uma tabela para cada subclasse
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE) // ira criar uma tabela e juntar todas as sublasses nela

public abstract class Pagamento implements Serializable {
// A classe Pagamento não poderá ser herdada!
	
	private static final long serialVersionUID = 3373226800245244855L;
	
	@Id
	@Column(name = "PagamentoId")
	private Integer pagamentoId;
	
	@Column(name = "EstadoPagamento")
	private Integer estadoPagamento;
	
	/**
	 * Pagamento não irá deserializar o pedido
	 */
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "PedidoId")
	@MapsId
	private Pedido pedido;
	/**
	 *  Pagamento terá o mesmo id de pedido, graças a anotação MapsId
	 */
	
	public Pagamento() {

	}

	public Pagamento(Integer pagamentoId, EstadoPagamento estadoPagamento, Pedido pedido) {
		super();
		this.pagamentoId = pagamentoId;
		this.estadoPagamento = (estadoPagamento == null) ? null : estadoPagamento.getCodigo();
		this.pedido = pedido;
	}

	public Integer getPagamentoId() {
		return pagamentoId;
	}

	public void setPagamentoId(Integer pagamentoId) {
		this.pagamentoId = pagamentoId;
	}

	public EstadoPagamento getEstadoPagamento() {
		return EstadoPagamento.toEnum(estadoPagamento);
	}

	public void setEstadoPagamento(EstadoPagamento estadoPagamento) {
		this.estadoPagamento = estadoPagamento.getCodigo();
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pagamentoId == null) ? 0 : pagamentoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pagamento other = (Pagamento) obj;
		if (pagamentoId == null) {
			if (other.pagamentoId != null)
				return false;
		} else if (!pagamentoId.equals(other.pagamentoId))
			return false;
		return true;
	}

}
