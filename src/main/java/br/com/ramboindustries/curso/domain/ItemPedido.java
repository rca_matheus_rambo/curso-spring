package br.com.ramboindustries.curso.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ItensPedidos")
public class ItemPedido implements Serializable {

	private static final long serialVersionUID = 738227398118461001L;

	/**
	 * O ID será embutido da classe ItemPedidoPK
	 */
	/**
	 * Irá ignorar totalmente o objeto itemPedidoPk
	 */
	@JsonIgnore
	@EmbeddedId
	private ItemPedidoPK itemPedidoPk = new ItemPedidoPK();

	@Column(name = "Desconto")
	private Double desconto;

	@Column(name = "Quantidade")
	private Integer quantidade;

	@Column(name = "Preco")
	private Double preco;

	public ItemPedido() {
	}

	/**
	 * Iremos setar o pedido e o produto do objeto itemPedidoPk passando o pedido e
	 * o produto pelo constutor
	 */
	public ItemPedido(Pedido pedido, Produto produto, Double desconto, Integer quantidade, Double preco) {
		super();
		itemPedidoPk.setPedido(pedido);
		itemPedidoPk.setProduto(produto);
		this.desconto = desconto;
		this.quantidade = quantidade;
		this.preco = preco;
	}

	/**
	 * Ignorar para evitar o Json Ciclico
	 */
	@JsonIgnore
	public Pedido getPedido() {
		return itemPedidoPk.getPedido();
	}
	
	
	public double getSubTotal() {
		return (preco - desconto) * quantidade;
	}


	public Produto getProduto() {
		return itemPedidoPk.getProduto();
	}

	public ItemPedidoPK getItemPedidoPk() {
		return itemPedidoPk;
	}

	public void setItemPedidoPk(ItemPedidoPK itemPedidoPk) {
		this.itemPedidoPk = itemPedidoPk;
	}

	public Double getDesconto() {
		return desconto;
	}

	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemPedidoPk == null) ? 0 : itemPedidoPk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemPedido other = (ItemPedido) obj;
		if (itemPedidoPk == null) {
			if (other.itemPedidoPk != null)
				return false;
		} else if (!itemPedidoPk.equals(other.itemPedidoPk))
			return false;
		return true;
	}
}
