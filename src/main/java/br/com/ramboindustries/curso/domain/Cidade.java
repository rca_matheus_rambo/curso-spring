package br.com.ramboindustries.curso.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Cidades")
public class Cidade implements Serializable {

	private static final long serialVersionUID = -2963545108838488077L;

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "CidadeId")
	private Integer cidadeId;

	@Column(name = "Nome")
	private String nome;

	@ManyToOne
	@JoinColumn(name = "EstadoId")
	private Estado estado;

	public Cidade() {

	}

	public Cidade(Integer cidadeId, String nome, Estado estado) {
		super();
		this.cidadeId = cidadeId;
		this.nome = nome;
		this.estado = estado;
	}

	public Integer getCidadeId() {
		return cidadeId;
	}

	public void setCidadeId(Integer cidadeId) {
		this.cidadeId = cidadeId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cidadeId == null) ? 0 : cidadeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cidade other = (Cidade) obj;
		if (cidadeId == null) {
			if (other.cidadeId != null)
				return false;
		} else if (!cidadeId.equals(other.cidadeId))
			return false;
		return true;
	}

}
