package br.com.ramboindustries.curso.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.ramboindustries.curso.domain.enums.EstadoPagamento;

@Entity
@Table(name = "PagamentosComBoleto")
public  class PagamentoComBoleto extends Pagamento {

	private static final long serialVersionUID = 5992816279859348029L;

	@Column(name = "DataVencimento")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dataVencimento;
	
	@Column(name = "DataPagamento")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dataPagamento;
	
	public PagamentoComBoleto() {
		
	}

	public PagamentoComBoleto(Integer pagamentoId, EstadoPagamento estadoPagamento, Pedido pedido, Date dataVencimento, Date dataPagamento) {
		super(pagamentoId, estadoPagamento, pedido);
		this.dataVencimento = dataVencimento;
		this.dataPagamento = dataPagamento;
		
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	
	
	
	

	
	
}
