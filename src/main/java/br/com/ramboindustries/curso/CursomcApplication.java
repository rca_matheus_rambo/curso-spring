package br.com.ramboindustries.curso;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.ramboindustries.curso.dao.CategoriaDao;
import br.com.ramboindustries.curso.dao.CidadeDao;
import br.com.ramboindustries.curso.dao.ClienteDao;
import br.com.ramboindustries.curso.dao.EnderecoDao;
import br.com.ramboindustries.curso.dao.EstadoDao;
import br.com.ramboindustries.curso.dao.ItemPedidoDao;
import br.com.ramboindustries.curso.dao.PagamentoDao;
import br.com.ramboindustries.curso.dao.PedidoDao;
import br.com.ramboindustries.curso.dao.ProdutoDao;
import br.com.ramboindustries.curso.domain.Categoria;
import br.com.ramboindustries.curso.domain.Cidade;
import br.com.ramboindustries.curso.domain.Cliente;
import br.com.ramboindustries.curso.domain.Endereco;
import br.com.ramboindustries.curso.domain.Estado;
import br.com.ramboindustries.curso.domain.ItemPedido;
import br.com.ramboindustries.curso.domain.Pagamento;
import br.com.ramboindustries.curso.domain.PagamentoComBoleto;
import br.com.ramboindustries.curso.domain.PagamentoComCartao;
import br.com.ramboindustries.curso.domain.Pedido;
import br.com.ramboindustries.curso.domain.Produto;
import br.com.ramboindustries.curso.domain.enums.EstadoPagamento;
import br.com.ramboindustries.curso.domain.enums.TipoCliente;

@SpringBootApplication
public class CursomcApplication implements CommandLineRunner {

	@Autowired
	private CategoriaDao categoriaDao;

	@Autowired
	private ProdutoDao produtoDao;

	@Autowired
	private EstadoDao estadoDao;

	@Autowired
	private CidadeDao cidadeDao;

	@Autowired
	private EnderecoDao enderecoDao;

	@Autowired
	private ClienteDao clienteDao;

	@Autowired
	private PedidoDao pedidoDao;

	@Autowired
	private PagamentoDao pagamentoDao;

	@Autowired
	private ItemPedidoDao itemPedidoDao;

	public static void main(String[] args) {
		SpringApplication.run(CursomcApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Categoria cat1 = new Categoria(null, "Informática");
		Categoria cat2 = new Categoria(null, "Escritório");
		Categoria cat3 = new Categoria(null, "Quarto");
		Categoria cat4 = new Categoria(null, "Cama, mesa e banho");
		Categoria cat5 = new Categoria(null, "Sala de estar");
		Categoria cat6 = new Categoria(null, "Cozinha");
		Categoria cat7 = new Categoria(null, "Jardinagem");

		Produto p1 = new Produto(null, "Computador", 2000.00);
 		Produto p2 = new Produto(null, "Impressora", 800.00);
 		Produto p3 = new Produto(null, "Mouse", 80.00);
		Produto p4 = new Produto(null, "Mesa de escritório", 300.00);
		Produto p5 = new Produto(null, "Toalha", 50.00);
		Produto p6 = new Produto(null, "Colcha", 200.00);
		Produto p7 = new Produto(null, "TV true color", 1200.00);
		Produto p8 = new Produto(null, "Roçadeira", 800.00);
		Produto p9 = new Produto(null, "Abajour", 100.00);
		Produto p10 = new Produto(null, "Pendente", 180.00);
		Produto p11 = new Produto(null, "Shampoo", 90.00);
		
		
		cat1.getProdutos().addAll(Arrays.asList(p1, p2, p3));
 		cat2.getProdutos().addAll(Arrays.asList(p2));
 		
 		p1.getCategorias().addAll(Arrays.asList(cat1));
 		p2.getCategorias().addAll(Arrays.asList(cat1, cat2));
 		p3.getCategorias().addAll(Arrays.asList(cat1));
 		
		cat2.getProdutos().addAll(Arrays.asList(p2, p4));
		cat3.getProdutos().addAll(Arrays.asList(p5, p6));
		cat4.getProdutos().addAll(Arrays.asList(p1, p2, p3, p7));
		cat5.getProdutos().addAll(Arrays.asList(p8));
		cat6.getProdutos().addAll(Arrays.asList(p9, p10));
		cat7.getProdutos().addAll(Arrays.asList(p11));
		
		p1.getCategorias().addAll(Arrays.asList(cat1, cat4));
		p2.getCategorias().addAll(Arrays.asList(cat1, cat2, cat4));
		p3.getCategorias().addAll(Arrays.asList(cat1, cat4));
		p4.getCategorias().addAll(Arrays.asList(cat2));
		p5.getCategorias().addAll(Arrays.asList(cat3));
		p6.getCategorias().addAll(Arrays.asList(cat3));
		p7.getCategorias().addAll(Arrays.asList(cat4));
		p8.getCategorias().addAll(Arrays.asList(cat5));
		p9.getCategorias().addAll(Arrays.asList(cat6));
		p10.getCategorias().addAll(Arrays.asList(cat6));
		p11.getCategorias().addAll(Arrays.asList(cat7));		
		
 		categoriaDao.saveAll(Arrays.asList(cat1, cat2, cat3, cat4, cat5, cat6, cat7));
		produtoDao.saveAll(Arrays.asList(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11));
 
		Estado est1 = new Estado(null, "São Paulo");
		Estado est2 = new Estado(null, "Rio Grande do Sul");
		Estado est3 = new Estado(null, "Rio de Janeiro");

		Cidade cid1 = new Cidade(null, "Porto Alegre", est2);
		Cidade cid2 = new Cidade(null, "Campinas", est1);
		Cidade cid3 = new Cidade(null, "Rio de Janeiro", est3);
		Cidade cid4 = new Cidade(null, "Novo Hamburgo", est2);
		Cidade cid5 = new Cidade(null, "São Paulo", est1);

		est1.getCidades().addAll(Arrays.asList(cid2, cid5));
		est2.getCidades().addAll(Arrays.asList(cid1, cid4));
		est3.getCidades().add(cid3);

		/**
		 * Cidade é dona do relacionamento Bidirecional, então salvamos ela por último!
		 */

		estadoDao.saveAll(Arrays.asList(est1, est2, est3));
		cidadeDao.saveAll(Arrays.asList(cid1, cid2, cid3, cid4, cid5));

		/**
		 * TipoCliente é um ENUM
		 */

		Cliente cli1 = new Cliente(null, "Matheus Rambo", "matheusrambo@gmail.com", "2504655",
				TipoCliente.PESSOA_FISICA);
		Cliente cli2 = new Cliente(null, "Lucas de Leão", "lucao@gmail.com", "251554", TipoCliente.PESSOA_FISICA);

		cli1.getTelefones().add("5165161");
		cli1.getTelefones().add("516516441");

		cli2.getTelefones().addAll(Arrays.asList("468464", "5616165161", "5614444484"));

		Endereco e1 = new Endereco(null, "Pau Brasil", "66", "casa", "Vale Verde", "65.56465", cli1, cid4);
		Endereco e2 = new Endereco(null, "Marcilio Dias", "6456", "Apartamento", "Nações Unidas", "4856465", cli1,
				cid4);
		Endereco e3 = new Endereco(null, "Pedro Mota", "140", "casa", "Pedro Alvaro", "84465", cli2, cid2);

		cli1.getEnderecos().addAll(Arrays.asList(e1, e2));
		cli2.getEnderecos().add(e3);

		clienteDao.saveAll(Arrays.asList(cli1, cli2));
		enderecoDao.saveAll(Arrays.asList(e1, e2, e3));

		//////////////////////////////////////////////////////////////////////////

		SimpleDateFormat sfd = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		Pedido ped1 = new Pedido(null, sfd.parse("25/01/2018 10:41"), cli1, e1);
		Pedido ped2 = new Pedido(null, sfd.parse("14/07/2018 08:44"), cli1, e2);

		Pagamento pg1 = new PagamentoComCartao(null, EstadoPagamento.QUITADO, ped1, 6);
		ped1.setPagamento(pg1);
		/**
		 * Associação Bidirecional
		 */

		Pagamento pg2 = new PagamentoComBoleto(null, EstadoPagamento.PENDENTE, ped2, sfd.parse("26/01/2018 00:00"),
				null);
		ped2.setPagamento(pg2);

		cli1.getPedidos().addAll((Arrays.asList(ped1, ped2)));

		/**
		 * Associa os pedidos na lista de pedidos do cliente
		 */

		/**
		 * Já que pedido é independente de pagamento, salvamos ele primeiro
		 */
		pedidoDao.saveAll(Arrays.asList(ped1, ped2));

		/**
		 * Pagamento depende de pedido
		 */
		pagamentoDao.saveAll(Arrays.asList(pg1, pg2));

		////////////////////////////////////////////////////////

		ItemPedido ip1 = new ItemPedido(ped1, p1, 20.0, 2, 2000.0);
		ItemPedido ip2 = new ItemPedido(ped1, p3, 0.0, 2, 80.0);
		ItemPedido ip3 = new ItemPedido(ped2, p2, 100.0, 1, 800.0);

		/**
		 * Realizando a associação Bidirecional entre A lista de itens do Pedido
		 */

		ped1.getItens().addAll(Arrays.asList(ip1, ip2));
		ped2.getItens().add(ip3);

		/**
		 * Realizando a associação bidirecioanl entre a lista de itends do Produto
		 */

		p1.getItens().add(ip1);
		p2.getItens().add(ip3);
		p3.getItens().add(ip2);

		itemPedidoDao.saveAll(Arrays.asList(ip1, ip2, ip3));

	}
}
