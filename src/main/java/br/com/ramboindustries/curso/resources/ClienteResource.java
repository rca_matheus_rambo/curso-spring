package br.com.ramboindustries.curso.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ramboindustries.curso.domain.Cliente;
import br.com.ramboindustries.curso.dto.ClienteDTO;
import br.com.ramboindustries.curso.dto.ClienteNovoDTO;
import br.com.ramboindustries.curso.service.ClienteService;

@RestController
@RequestMapping(value = "/clientes")
public class ClienteResource {

	@Autowired
	private ClienteService clienteService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Cliente> find(@PathVariable("id") Integer id) {
		return ResponseEntity.ok().body(clienteService.find(id));
	}

	/**
	 * @return uma lista de ClienteDTO
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ClienteDTO>> findAll() {
		return ResponseEntity.ok().body(clienteService.findAll());
	}

	@PostMapping
	public ResponseEntity<Void> save(@Valid @RequestBody ClienteNovoDTO clienteNovoDTO) {
		clienteService.save(clienteNovoDTO);
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@PathVariable(value = "id") Integer id,
			@Valid @RequestBody ClienteDTO clienteDTO) {
		clienteDTO.setClienteId(id);
		clienteService.update(clienteDTO);
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ResponseEntity<Page<ClienteDTO>> findPage(@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "lines", defaultValue = "24") Integer linesPerPage,
			@RequestParam(value = "order", defaultValue = "nome") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction) {
		Page<Cliente> clientes = clienteService.findPage(page, linesPerPage, orderBy, direction);
		Page<ClienteDTO> clienteDTO = clientes.map(cliente -> new ClienteDTO(cliente));
		return ResponseEntity.ok().body(clienteDTO);
	}

	@DeleteMapping
	public ResponseEntity<Void> delete(@RequestParam(value = "id") Integer id) {
		clienteService.delete(id);
		return ResponseEntity.noContent().build();
	}

}
