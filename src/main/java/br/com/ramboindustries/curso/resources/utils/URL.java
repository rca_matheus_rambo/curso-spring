package br.com.ramboindustries.curso.resources.utils;

import java.util.ArrayList;
import java.util.List;

public class URL {

	/**
	 * Ao passar uma string, ira separar os ids das virgulas
	 * 
	 * @param stringList
	 *            1,5,6,9,10,25
	 * @return Uma lista 1 5 6 10 25
	 */

	public static List<Integer> decodeStringToList(String stringList) {
		StringBuilder builder = new StringBuilder();
		builder.append(stringList.replaceAll(" ", "").trim());
		builder.append(",");
		int length = builder.length();
		int last = 0;
		List<Integer> ids = new ArrayList<Integer>();

		for (int i = 0; i < length; i++) {
			if (builder.toString().charAt(i) == ',') {
				ids.add(Integer.parseInt(builder.toString().substring(last, i)));
				last = i + 1;
			}
		}
		return ids;
	}
	/*
	 * public static List<Integer> decodeInt(String s){ s = s.replace(" ", "");
	 * String [] v = s.split(","); List<Integer> id = new ArrayList<Integer>();
	 * for(int i = 0; i < v.length; i++) { id.add(Integer.parseInt(v[i])); } return
	 * id; }
	 */

	public static String decodeParam(String param) {
		StringBuilder builder = new StringBuilder();
		int position = 0;
		if (param.contains("?")) {
			position = param.indexOf("?");
			builder.append(param.substring(0, position).replace(" ", "%"));
			builder.append(param.substring(position, param.length()).replace(" ", "+"));
			return builder.toString();
		} else {
			return param.replace(" ", "%");
		}
	}

	/**
	public static String decodeParam(String param) {
		return URLDecoder.decode(param, "UTF-8");
	}
	*/

}
