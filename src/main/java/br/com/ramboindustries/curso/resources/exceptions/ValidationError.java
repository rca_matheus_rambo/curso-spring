package br.com.ramboindustries.curso.resources.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ValidationError extends StandardError {

	private static final long serialVersionUID = -2560240199822527154L;

	private List<FieldMessage> fieldMessages = new ArrayList<>();

	public ValidationError(Integer status, String msg, Long timeStamp) {
		super(status, msg, timeStamp);
	}
	
	/**
	 * O java irá retornar no JSON com o nome de Error!
	 */
	public List<FieldMessage> getError() {
		return fieldMessages;
	}

	public void addFieldMessages(FieldMessage fieldMessage) {
		fieldMessages.add(fieldMessage);
	}

	public void addFieldMessages(String fieldName, String message) {
		fieldMessages.add(new FieldMessage(fieldName, message));
	}

}
