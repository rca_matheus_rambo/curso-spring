package br.com.ramboindustries.curso.resources.exceptions;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.ramboindustries.curso.service.exceptions.DataIntegrityException;
import br.com.ramboindustries.curso.service.exceptions.ObjectNotFoundException;

@ControllerAdvice
public class ResourceExceptionHandler {

	@ExceptionHandler(ObjectNotFoundException.class)
	public ResponseEntity<StandardError> objectNotFound(ObjectNotFoundException ex, HttpServletRequest http){
		StandardError standardError = new StandardError(HttpStatus.NOT_FOUND.value() , ex.getMessage(), System.currentTimeMillis());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(standardError);
	} 
	
	@ExceptionHandler(DataIntegrityException.class)
	public ResponseEntity<StandardError> dataIntegrity(DataIntegrityException ex, HttpServletRequest http){
		StandardError standardError = new StandardError(HttpStatus.BAD_REQUEST.value() , ex.getMessage(), System.currentTimeMillis());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(standardError);
	} 
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<StandardError> validation(MethodArgumentNotValidException ex, HttpServletRequest http){
		ValidationError validationError = new ValidationError(HttpStatus.BAD_REQUEST.value() , "Erro de Validação!", System.currentTimeMillis());
		
		/** 
		 * Pega a lista de erros que foram retornados 
		 */
		for(FieldError x : ex.getBindingResult().getFieldErrors()) {
			validationError.addFieldMessages(x.getField(),x.getDefaultMessage());
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validationError);
	} 
	
	
	
}
