package br.com.ramboindustries.curso.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.ramboindustries.curso.domain.Categoria;
import br.com.ramboindustries.curso.dto.CategoriaDTO;
import br.com.ramboindustries.curso.service.CategoriaService;

@RestController
@RequestMapping(value = "/categorias")
public class CategoriaResource {

	@Autowired
	private CategoriaService categoriaService;

	/**
	 * @GetMapping(value = "/{id}") é um atalho para
	 * @RequestMapping(value = "/{id}", method = RequestMethod.GET)
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Categoria> find(@PathVariable("id") Integer id) {
		return ResponseEntity.ok().body(categoriaService.find(id));
	}

	/**
	 * @PostMapping é um atalho para
	 * @RequestMapping(method = RequestMathod.POST)
	 */
	/**
	 * A anotação @Valid irá validar!
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> save(@Valid @RequestBody CategoriaDTO categoriaDTO) {
		Categoria categoria = categoriaService.save(categoriaDTO);
		URI uri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{id}")
				.buildAndExpand(categoria.getCategoriaId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	/**
	 * @PutMapping(value = "/{id}") é um atalho para o
	 * @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@PathVariable(value="id") Integer id,
			@Valid @RequestBody CategoriaDTO categoriaDTO) {
		categoriaDTO.setCategoriaId(id);
		categoriaService.update(categoriaDTO);
		return ResponseEntity.noContent().build();
	}

	/**
	 * @DeleteMapping(value = "/{id}") é uma atalho para
	 * @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@RequestParam(value="id") Integer id) {
		categoriaService.delete(id);
		return ResponseEntity.noContent().build();
	}

	/**
	 * Irá retornar uma lista de categorias então, usamos: <List<CategoriaDTO>>
	 */
	@GetMapping
	public ResponseEntity<List<CategoriaDTO>> findAll() {
		return ResponseEntity.ok().body(categoriaService.findAll());
	}

	/**
	 * Irá retornar uma paginaçao, de acordo com os parametros passados na
	 * requisição caso os parametros não sejam informados, iremos usar valores
	 * definidos como defaultValue
	 */
	@GetMapping(value = "/page")
	public ResponseEntity<Page<CategoriaDTO>> findPage(@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "lines", defaultValue = "24") Integer linesPerPage,
			@RequestParam(value = "orderBy", defaultValue = "nome") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction) {
		Page<Categoria> categorias = categoriaService.findPage(page, linesPerPage, orderBy, direction);
		Page<CategoriaDTO> categoriasDTO = categorias.map(categoria -> new CategoriaDTO(categoria));
		return ResponseEntity.ok().body(categoriasDTO);
	}

}
