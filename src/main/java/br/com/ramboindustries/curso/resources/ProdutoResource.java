package br.com.ramboindustries.curso.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ramboindustries.curso.domain.Produto;
import br.com.ramboindustries.curso.dto.ProdutoDTO;
import br.com.ramboindustries.curso.resources.utils.URL;
import br.com.ramboindustries.curso.service.ProdutoService;

@RestController
@RequestMapping(value = "/produtos")
public class ProdutoResource {

	@Autowired
	private ProdutoService produtoService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Produto> find(@PathVariable(value = "id") Integer id) {
		return ResponseEntity.ok().body(produtoService.find(id));
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ProdutoDTO>> findAll() {
		return ResponseEntity.ok().body(produtoService.findAll());
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> save(@RequestBody ProdutoDTO produtoDTO) {
		produtoService.save(produtoDTO);
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@RequestParam(value = "id") Integer id) {
		produtoService.delete(id);
		return ResponseEntity.noContent().build();
	}

	/**
	 * Como estamos usando o Like %nome% ao inserirmos qualquer letra, ele irá nos retornar o que 
	 * está relacionado com o nome
	 * Exe: ?nome=me
	 * retorna Mesa de Escritório e muito mais
	 * Categorias passar como 1,2,6
	 * tem um método que corta as virgulas e então pega somente os ids
	 * e joga para uma lista de inteiros
	 */
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ResponseEntity<Page<ProdutoDTO>> findPage(@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "lines", defaultValue = "24") Integer linesPerPage,
			@RequestParam(value = "order", defaultValue = "nome") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction,
			@RequestParam(value = "nome", defaultValue = "") String nome,
			@RequestParam(value = "categorias", defaultValue = "0") String categorias) {
		List<Integer> categoriasIds = URL.decodeStringToList(categorias);
		String nomeDecoded = URL.decodeParam(nome);
		Page<Produto> pageProduto = produtoService.search(nomeDecoded, categoriasIds, page, linesPerPage, orderBy,
				direction);
		Page<ProdutoDTO> pageProdutoDTO = pageProduto.map(produto -> new ProdutoDTO(produto));
		return ResponseEntity.ok().body(pageProdutoDTO);

	}

}
