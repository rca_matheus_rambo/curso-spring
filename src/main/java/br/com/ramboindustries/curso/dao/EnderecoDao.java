package br.com.ramboindustries.curso.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ramboindustries.curso.domain.Endereco;

@Repository
public interface EnderecoDao extends JpaRepository<Endereco, Integer> {

}
