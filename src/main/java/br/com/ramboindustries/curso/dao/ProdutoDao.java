package br.com.ramboindustries.curso.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.ramboindustries.curso.domain.Categoria;
import br.com.ramboindustries.curso.domain.Produto;

@Repository
@Transactional(readOnly = true)
public interface ProdutoDao extends JpaRepository<Produto, Integer> {

	/**
	 * Como é uma interface, podemos criar uma classe e então implementar está
	 * interface, e então, sobreescrevermos o método search, mas o framework nos
	 * possibilita utilizar a anotação @Query, que é em formato JPQL. O framework
	 * irá fazer o pré-processamento e implementar o método! A
	 * anotação @Param("atributo") é a mesma do query.setParameter("atributo",
	 * valor) o framework faz a implementação automaticamente.
	 */
	@Transactional(readOnly = true)
	@Query("SELECT DISTINCT p FROM Produto p INNER JOIN p.categorias c WHERE p.nome LIKE %:nome% AND c IN :categorias")
	public Page<Produto> search(@Param("nome") String nome, @Param("categorias") List<Categoria> categorias,
			Pageable pageRequest);

}
