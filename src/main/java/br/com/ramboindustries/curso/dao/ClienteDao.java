package br.com.ramboindustries.curso.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.ramboindustries.curso.domain.Cliente;

@Repository
public interface ClienteDao extends JpaRepository<Cliente, Integer> {

	/**
	* O próprio Spring irá realizar o método para nós!
	 */
	@Transactional(readOnly = true)
	Cliente findByEmail(String email);
	
}
