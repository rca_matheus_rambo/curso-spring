package br.com.ramboindustries.curso.dto;

import java.io.Serializable;

import br.com.ramboindustries.curso.domain.Produto;

public class ProdutoDTO implements Serializable{

	private static final long serialVersionUID = -8325135013735149585L;
	private Integer produtoId;
	private String nome;
	private Double preco;
	
	
	public ProdutoDTO() {
		
	}
	
	public ProdutoDTO(Produto produto) {
		produtoId = produto.getProdutoId();
		nome = produto.getNome();
		preco = produto.getPreco();
	}


	public ProdutoDTO(Integer produtoId, String nome, Double preco) {
		super();
		this.produtoId = produtoId;
		this.nome = nome;
		this.preco = preco;
	}


	public Integer getProdutoId() {
		return produtoId;
	}


	public void setProdutoId(Integer produtoId) {
		this.produtoId = produtoId;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public Double getPreco() {
		return preco;
	}


	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	
	
}
