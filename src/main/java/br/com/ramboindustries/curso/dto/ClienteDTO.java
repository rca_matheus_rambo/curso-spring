package br.com.ramboindustries.curso.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import br.com.ramboindustries.curso.domain.Cliente;
import br.com.ramboindustries.curso.service.validation.ClientUpdate;

@ClientUpdate
public class ClienteDTO implements Serializable {

	private static final long serialVersionUID = -8452758625836955367L;

	private Integer clienteId;
	@NotEmpty(message = "O campo não pode ser vazio!")
	@Length(min = 5,max = 100, message = "O nome deve estar entre: 5 e 100 caracteres!")
	private String nome;
	
	@Email(message = "e-mail inválido") 
	@NotEmpty(message = "O e-mail não pode ser vazio!")
	private String email;

	public ClienteDTO() {

	}

	public ClienteDTO(Cliente cliente) {
		clienteId = cliente.getClienteId();
		nome = cliente.getNome();
		email = cliente.getEmail();
	}

	public Integer getClienteId() {
		return clienteId;
	}

	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "ClienteDTO [clienteId=" + clienteId + ", nome=" + nome + ", email=" + email + "]";
	}

}
