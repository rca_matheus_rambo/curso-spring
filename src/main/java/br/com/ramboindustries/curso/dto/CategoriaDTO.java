package br.com.ramboindustries.curso.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import br.com.ramboindustries.curso.domain.Categoria;

/**
 * usamos o DTO quando queremos trafegar operações básicas de determinada classe
 */
public class CategoriaDTO implements Serializable {

	private static final long serialVersionUID = -6618542687609009378L;

	private Integer categoriaId;

	/**
	 * Anotação para não permitir String vazia
	 */
	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(min = 5, max = 80, message = "O tamanho deve ser entre 5 e 80 caracteres")
	private String nome;

	public CategoriaDTO() {

	}

	public CategoriaDTO(Integer categoriaId, String nome) {
		super();
		this.categoriaId = categoriaId;
		this.nome = nome;
	}

	public CategoriaDTO(Categoria categoria) {
		categoriaId = categoria.getCategoriaId();
		nome = categoria.getNome();
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
