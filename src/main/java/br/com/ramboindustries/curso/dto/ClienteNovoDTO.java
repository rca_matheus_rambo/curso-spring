package br.com.ramboindustries.curso.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.ramboindustries.curso.service.validation.ClientInsert;

@ClientInsert
public class ClienteNovoDTO implements Serializable {

	private static final long serialVersionUID = 8857706720787548801L;

	private final String NOT_EMPTY = "não pode ser vazio!";
	private final String NOT_NULL = "não pode ser nulo!";

	@NotEmpty(message = NOT_EMPTY)
	@NotNull(message = NOT_NULL)
	private String nome;

	@NotEmpty(message = NOT_EMPTY)
	@Email(message = "e-mail inválido!")
	private String email;

	@NotEmpty(message = NOT_EMPTY)
	@NotNull(message = NOT_NULL)
	private String cpfOuCnpj;

	private Integer tipoCliente;

	@NotNull(message = NOT_NULL)
	private String logradouro;

	@NotNull(message = NOT_NULL)
	private String numero;

	@NotNull(message = NOT_NULL)
	private String complemento;

	@NotNull(message = NOT_NULL)
	private String bairro;

	@NotNull(message = NOT_NULL)
	private String cep;

	@NotNull(message = NOT_NULL)
	private Integer cidadeId;

	List<String> telefones = new ArrayList<>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpfOuCnpj() {
		return cpfOuCnpj;
	}

	public void setCpfOuCnpj(String cpfOuCnpj) {
		this.cpfOuCnpj = cpfOuCnpj;
	}

	public Integer getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(Integer tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public Integer getCidadeId() {
		return cidadeId;
	}

	public void setCidadeId(Integer cidadeId) {
		this.cidadeId = cidadeId;
	}

}
