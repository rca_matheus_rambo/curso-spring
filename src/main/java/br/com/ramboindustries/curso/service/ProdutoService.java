package br.com.ramboindustries.curso.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.ramboindustries.curso.dao.CategoriaDao;
import br.com.ramboindustries.curso.dao.ProdutoDao;
import br.com.ramboindustries.curso.domain.Categoria;
import br.com.ramboindustries.curso.domain.Produto;
import br.com.ramboindustries.curso.dto.ProdutoDTO;
import br.com.ramboindustries.curso.service.exceptions.DataIntegrityException;
import br.com.ramboindustries.curso.service.exceptions.ObjectNotFoundException;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoDao produtoDao;

	@Autowired
	private CategoriaDao categoriaDao;

	public Produto find(Integer id) {
		Optional<Produto> optional = produtoDao.findById(id);
		return optional
				.orElseThrow(() -> new ObjectNotFoundException("Produto do código: " + id + " não foi encontrado!"));
	}

	public Produto save(ProdutoDTO produtoDTO) {
		Produto produto = fromDTO(produtoDTO);
		return produtoDao.save(produto);
	}

	public List<ProdutoDTO> findAll() {
		List<Produto> produtos = produtoDao.findAll();
		List<ProdutoDTO> produtosDTO = produtos.stream().map(produto -> new ProdutoDTO(produto))
				.collect(Collectors.toList());
		return produtosDTO;
	}

	public Page<Produto> search(String nome, List<Integer> categoriasId, Integer page, Integer linesPerPage,
			String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		List<Categoria> categorias = categoriaDao.findAllById(categoriasId);
		return produtoDao.search(nome, categorias, pageRequest);
	}

	public void delete(Integer id) {
		try {
			produtoDao.deleteById(id);
		} catch (DataIntegrityViolationException ex) {
			throw new DataIntegrityException("O produto possui relações!");
		}
	}

	private Produto fromDTO(ProdutoDTO produtoDTO) {
		return new Produto(produtoDTO.getProdutoId(), produtoDTO.getNome(), produtoDTO.getPreco());
	}

}
