package br.com.ramboindustries.curso.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.ramboindustries.curso.dao.CategoriaDao;
import br.com.ramboindustries.curso.domain.Categoria;
import br.com.ramboindustries.curso.dto.CategoriaDTO;
import br.com.ramboindustries.curso.service.exceptions.DataIntegrityException;
import br.com.ramboindustries.curso.service.exceptions.ObjectNotFoundException;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaDao categoriaDao;

	public Categoria find(Integer id) {
		Optional<Categoria> optional = categoriaDao.findById(id);
		return optional.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado!"));
	}

	public Categoria save(CategoriaDTO categoriaDTO) {
		return categoriaDao.save(fromDTO(categoriaDTO));
	}

	public Categoria update(CategoriaDTO categoriaDTO) {
		Categoria categoria = fromDTO(categoriaDTO);
		return categoriaDao.save(categoria);
	}

	public void delete(Integer id) {
		try {
			categoriaDao.deleteById(id);
		} catch (DataIntegrityViolationException ex) {
			throw new DataIntegrityException("A categoria possui produtos relacionados!");
		}
	}

	public List<CategoriaDTO> findAll() {
		List<Categoria> categorias = categoriaDao.findAll();
		List<CategoriaDTO> categoriasDTO = categorias.stream().map(categoria -> new CategoriaDTO(categoria))
				.collect(Collectors.toList());
		return categoriasDTO;
	}

	/**
	 * Retorna uma página
	 * 
	 * @param page
	 *            página inicial
	 * @param linesPerPage
	 *            linhas por página
	 * @param orderBy
	 *            ordenar por
	 * @param direction
	 *            descendente ou crescente
	 * @return
	 */
	public Page<Categoria> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return categoriaDao.findAll(pageRequest);
	}

	private Categoria fromDTO(CategoriaDTO categoriaDTO) {
		return new Categoria(categoriaDTO.getCategoriaId(), categoriaDTO.getNome());
	}

}
