package br.com.ramboindustries.curso.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ramboindustries.curso.dao.PedidoDao;
import br.com.ramboindustries.curso.domain.Pedido;
import br.com.ramboindustries.curso.service.exceptions.ObjectNotFoundException;

@Service
public class PedidoService {

	@Autowired
	private PedidoDao pedidoDao;
	
	public Pedido find(Integer id){
		Optional<Pedido> pedido = pedidoDao.findById(id);
		return pedido.orElseThrow(() -> new ObjectNotFoundException("Não encontramos o pedido de id: " + id));
	}
	
	
}
