package br.com.ramboindustries.curso.service.validation.util;

public class BRCnpj {

	private static int getFirstDigit(String cnpj) {
		return Integer.parseInt(cnpj.substring(cnpj.length() - 2, cnpj.length() - 1));
	}

	private static int getSecondDigit(String cnpj) {
		return Integer.parseInt(cnpj.substring(cnpj.length() - 1, cnpj.length()));

	}

	private static int firstDigit(String cnpj) {
		int x = 5;
		int sum = 0;
		for (int i = 0; i < cnpj.length() - 2; i++) {
			sum += x-- * Integer.parseInt(cnpj.substring(i, i + 1));
			if (x < 2)
				x = 9;
		}
		int mod = sum % 11;
		return mod < 2 ? 0 : 11 - mod;
	}

	private static int secondDigit(String cnpj) {
		int x = 6;
		int sum = 0;
		for (int i = 0; i < cnpj.length() - 2; i++) {
			sum += x-- * Integer.parseInt(cnpj.substring(i, i + 1));
			if (x < 2)
				x = 9;
		}
		int mod = (sum + (firstDigit(cnpj) * 2)) % 11;
		return mod < 2 ? 0 : 11 - mod;
	}

	public static boolean isValid(String cnpj) {
		return getFirstDigit(cnpj) == firstDigit(cnpj) && getSecondDigit(cnpj) == secondDigit(cnpj);
	}

}
