package br.com.ramboindustries.curso.service.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import br.com.ramboindustries.curso.dao.ClienteDao;
import br.com.ramboindustries.curso.domain.Cliente;
import br.com.ramboindustries.curso.dto.ClienteDTO;
import br.com.ramboindustries.curso.resources.exceptions.FieldMessage;

public class ClientUpdateValidator implements ConstraintValidator<ClientUpdate, ClienteDTO> {

	@Autowired
	private ClienteDao clienteDao;

	/**
	 * Usaremos o HtpServletRequest para pegarmos o ID do cliente que iremos
	 * alterar. Ele será a nossa requisição.
	 */
	@Autowired
	private HttpServletRequest httpServletRequest;

	@Override
	public void initialize(ClientUpdate clientUpdate) {

	}

	@Override
	public boolean isValid(ClienteDTO clienteDTO, ConstraintValidatorContext context) {
		List<FieldMessage> listaErros = new ArrayList<>();

		/**
		 * A interface HandlerMapping, serve para manusear requisições e objetos.
		 * A constante URI_TEMPLATE_VARIABLES_ATTRIBUTE contém os atributos e valores da URI.
		 */
		@SuppressWarnings("unchecked")
		Map<String, String> map = (Map<String, String>) httpServletRequest.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);

		/**
		 * O get da interface map, requer uma chave passada como parametro, e retorna o valor desta chave!
		 * ou seja, irá retornar o valor que foi setado!
		 */
	
		Integer uriID = Integer.valueOf(map.get("id"));
			
		Cliente cliente = clienteDao.findByEmail(clienteDTO.getEmail());
		if (cliente != null && !cliente.getClienteId().equals(uriID)) {
			listaErros.add(new FieldMessage("email","Email pertence ao: " + cliente.getNome()));
		}

		for (FieldMessage fieldMessage : listaErros) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(fieldMessage.getMessage())
					.addPropertyNode(fieldMessage.getFieldName()).addConstraintViolation();
		}

		return listaErros.isEmpty();
	}
}
