package br.com.ramboindustries.curso.service.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.ramboindustries.curso.dao.ClienteDao;
import br.com.ramboindustries.curso.domain.Cliente;
import br.com.ramboindustries.curso.domain.enums.TipoCliente;
import br.com.ramboindustries.curso.dto.ClienteNovoDTO;
import br.com.ramboindustries.curso.resources.exceptions.FieldMessage;
import br.com.ramboindustries.curso.service.validation.util.BRCnpj;
import br.com.ramboindustries.curso.service.validation.util.BRCpf;

public class ClientInsertValidator implements ConstraintValidator<ClientInsert, ClienteNovoDTO> {

	@Autowired
	private ClienteDao clienteDao;
	
	
	@Override
	public void initialize(ClientInsert clientInsert) {
		
	}
	
	@Override
	public boolean isValid(ClienteNovoDTO clienteNovoDTO, ConstraintValidatorContext context) {
		List<FieldMessage> listaErros = new ArrayList<>();
		
		if(clienteNovoDTO.getTipoCliente().equals(TipoCliente.PESSOA_FISICA.getCodigo()) &&  !BRCpf.isValidCPF(clienteNovoDTO.getCpfOuCnpj())) {
			listaErros.add(new FieldMessage("cpfOuCnpj", "CPF inválido!"));
		}
		
		if(clienteNovoDTO.getTipoCliente().equals(TipoCliente.PESSOA_JURIDICA.getCodigo()) && !BRCnpj.isValid(clienteNovoDTO.getCpfOuCnpj()) ) {
			listaErros.add(new FieldMessage("cpfOuCnpj", "CNPJ inválido!"));
		}
		
		Cliente cliente = clienteDao.findByEmail(clienteNovoDTO.getEmail());
		if(cliente != null) {
			listaErros.add(new FieldMessage("email", "e-mail já está em uso!"));
		}else {
			System.out.println("OI");
		}
		
		
		for(FieldMessage fieldMessage : listaErros) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(fieldMessage.getMessage()).addPropertyNode(fieldMessage.getFieldName()).addConstraintViolation();
		}
		
		
		return listaErros.isEmpty();
	}	
}
