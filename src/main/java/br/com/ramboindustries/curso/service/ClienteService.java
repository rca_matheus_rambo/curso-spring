package br.com.ramboindustries.curso.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ramboindustries.curso.dao.ClienteDao;
import br.com.ramboindustries.curso.dao.EnderecoDao;
import br.com.ramboindustries.curso.domain.Cidade;
import br.com.ramboindustries.curso.domain.Cliente;
import br.com.ramboindustries.curso.domain.Endereco;
import br.com.ramboindustries.curso.domain.enums.TipoCliente;
import br.com.ramboindustries.curso.dto.ClienteDTO;
import br.com.ramboindustries.curso.dto.ClienteNovoDTO;
import br.com.ramboindustries.curso.service.exceptions.DataIntegrityException;
import br.com.ramboindustries.curso.service.exceptions.ObjectNotFoundException;

@Service
public class ClienteService {

	@Autowired
	private ClienteDao clienteDao;

	@Autowired
	private EnderecoDao enderecoDao;

	public Cliente find(Integer id) {
		Optional<Cliente> optional = clienteDao.findById(id);
		return optional.orElseThrow(() -> new ObjectNotFoundException("Cliente do código: " + id + " não foi encontrado!"));
	}

	public List<ClienteDTO> findAll() {
		List<Cliente> clientes = clienteDao.findAll();
		List<ClienteDTO> clientesDTO = clientes.stream().map(cliente -> new ClienteDTO(cliente))
				.collect(Collectors.toList());
		return clientesDTO;
	}

	/**
	 * Irá salvar tantos os endereços como o cliente na mesma transação
	 */
	@Transactional
	public Cliente save(ClienteNovoDTO clienteNovoDTO) {
		Cliente cliente = fromDTO(clienteNovoDTO);
		cliente = clienteDao.save(cliente);
		enderecoDao.saveAll(cliente.getEnderecos());
		return cliente;
	}

	public Cliente update(ClienteDTO clienteDTO) {
		Cliente cliente = find(clienteDTO.getClienteId());
		cliente.setEmail(clienteDTO.getEmail());
		cliente.setNome(clienteDTO.getNome());
		return clienteDao.save(cliente);

	}

	public void delete(Integer id) {
		try {
			clienteDao.deleteById(id);
		} catch (DataIntegrityViolationException ex) {
			throw new DataIntegrityException("O cliente possui relações!");
		}
	}

	public Page<Cliente> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return clienteDao.findAll(pageRequest);
	}

	public Cliente fromDTO(ClienteDTO clienteDTO) {
		return new Cliente(clienteDTO.getClienteId(), clienteDTO.getNome(), clienteDTO.getEmail(), null, null);
	}

	private Cliente fromDTO(ClienteNovoDTO clienteNovoDTO) {
		Cliente cliente = new Cliente(null, clienteNovoDTO.getNome(), clienteNovoDTO.getEmail(),
				clienteNovoDTO.getCpfOuCnpj(), TipoCliente.toEnum(clienteNovoDTO.getTipoCliente()));
		Cidade cidade = new Cidade(clienteNovoDTO.getCidadeId(), null, null);
		Endereco endereco = new Endereco(null, clienteNovoDTO.getLogradouro(), clienteNovoDTO.getNumero(),
				clienteNovoDTO.getComplemento(), clienteNovoDTO.getBairro(), clienteNovoDTO.getCep(), cliente, cidade);
		cliente.getEnderecos().add(endereco);
		cliente.getTelefones().addAll(clienteNovoDTO.getTelefones());
		return cliente;

	}

}
