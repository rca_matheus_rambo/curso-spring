package br.com.ramboindustries.curso.service.validation.util;

public final class BRCpf {

	private static final int firstDigit(String cpf) {
		int x = 10;
		int sum = 0;
		for (int i = 0; i < cpf.length() - 2; i++)
			sum += (x-- * Integer.parseInt(cpf.substring(i, i + 1)));
		int value = sum % 11;
		return value == 0 || value == 1 ? 0 : 11 - value;
	}
	
	private static int secondDigit(String cpf) {
		int x = 11;
		int sum = 0;
		for (int i = 0; i < cpf.length() - 2; i++)
			sum += (x-- * Integer.parseInt(cpf.substring(i, i + 1)));
		sum = firstDigit(cpf) * x + sum;
		int value = sum % 11;
		return value == 0 || value == 1 ? 0 : 11 - value;
	}

	private static final int getFirstDigit(String cpf) {
		return Integer.parseInt(cpf.substring(cpf.length() - 2, cpf.length() - 1));
	}

	private static final int getSecondDigit(String cpf) {
		return Integer.parseInt(cpf.substring(cpf.length() - 1, cpf.length()));
	}

	public static final boolean isValidCPF(String cpf) {
		if(cpf.length() != 11) return false;
		return getFirstDigit(cpf) == firstDigit(cpf) && getSecondDigit(cpf) == secondDigit(cpf);
	}	
	
}
